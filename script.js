function main() {
  let userInput;
  let isNumber = false;

  while (!isNumber) {
    userInput = prompt("Введіть число для обчислення факторіалу:");
    isNumber = isNumeric(userInput);

    if (!isNumber) {
      alert("Ви ввели не число. Спробуйте ще раз.");
    }
  }

  const number = parseInt(userInput);
  const result = factorial(number);

  alert(`Факторіал числа ${number} дорівнює: ${result}`);
}

function isNumeric(input) {
  return (
    !isNaN(input) && isFinite(input) && Number.isInteger(parseFloat(input))
  );
}

function factorial(f) {
  if (f === 0) {
    return 1;
  }
  return f * factorial(f - 1);
}

main();
